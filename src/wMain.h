#ifndef __WMAIN_H__
#define __WMAIN_H__

#include <FL/Fl.H>
#include <FL/Fl_Sys_Menu_Bar.H>
#include <FL/Fl_Menu_Bar.H>
#include <FL/Fl_Scroll.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Double_Window.H>

#include <list>

class wMain
{
    public:
        wMain( int argc, char** argv );
        virtual ~wMain();

    public:
        int     Run();
        void    WidgetCB( Fl_Widget* w );
        void    MenuCB( Fl_Widget* w );
        void    MoveCB( Fl_Widget* w );

    protected:
        void parseParams();
        void createComponents();
        void applyThemes();
        void arrangeChildWindows();
        void new_image();
        void open_image();
        void close_image();

    protected:
        int                 _argc;
        char**              _argv;

    protected:
        Fl_Double_Window*   mainWindow;
        Fl_Menu_Bar*        sysMenu;
        Fl_Scroll*          sclClient;
        Fl_Group*           grpStatus;
        Fl_Double_Window*   histWindow;
        Fl_Double_Window*   hdrWindowDrago;
        Fl_Double_Window*   hdrWindowReinhard;
};

#endif /// of __WINMAIN_H__
