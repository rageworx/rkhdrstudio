#include "themes.h"

////////////////////////////////////////////////////////////////////////////////

const int default_font_size     = 12;
const Fl_Color color_bg_base    = 0x33333300;
const Fl_Color color_bg_group   = 0x15151500;
const Fl_Color color_fg_base    = 0xCCCCCC00;
const Fl_Color color_fg_menu    = 0x99999900;

////////////////////////////////////////////////////////////////////////////////

void rkhdr::InitTheme()
{
    // Something do.
}

void rkhdr::ApplyDWindowTheme( Fl_Double_Window* w )
{
    if ( w != NULL )
    {
        w->color( color_bg_base );
        w->labelcolor( color_fg_base );
        w->labelsize( default_font_size );
    }
}

void rkhdr::ApplyDefaultTheme( Fl_Widget* w )
{
    if ( w != NULL )
    {
        w->color( color_bg_base );
        w->labelcolor( color_fg_base );
        w->labelsize( default_font_size );
    }
}

void rkhdr::ApplyButtonTheme( Fl_Widget* w )
{
    if ( w != NULL )
    {
        w->box( FL_THIN_UP_BOX );
        ApplyDefaultTheme( w );
    }
}

void rkhdr::ApplyGroupTheme( Fl_Group* w )
{
    if ( w != NULL )
    {
        ApplyDefaultTheme( w );
        w->color( color_bg_group );
    }
}

void rkhdr::ApplyScrollTheme( Fl_Scroll* w )
{
    if ( w != NULL )
    {
        ApplyDefaultTheme( w );
        w->color( color_bg_group );
        w->scrollbar_size( 5 );
    }
}

void rkhdr::ApplyMenuBarTheme( Fl_Menu_Bar* w )
{
    if ( w != NULL )
    {
        ApplyDefaultTheme( w );
        w->box( FL_FLAT_BOX );
        w->textcolor( color_fg_menu );
        w->textsize( default_font_size );
        w->redraw();
    }
}
