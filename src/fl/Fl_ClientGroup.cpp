#ifdef _WIN32
    #include <windows.h>
#endif // _WIN32

#include <FL/fl_draw.H>
#include "Fl_ClientGroup.H"

////////////////////////////////////////////////////////////////////////////////

#define DEF_Fl_ClientGroup_ICON_BOX_WH              20
#define DEF_Fl_ClientGroup_LABEL_SIZE               9
#define DEF_FL_BWINDOW_SYS_BTN_TEXT_HIDE            "@2>|"
#define DEF_FL_BWINDOW_SYS_BTN_TEXT_MAXIMIZE        "@8>|"
#define DEF_FL_BWINDOW_SYS_BTN_TEXT_RETURNSIZE      "@-1undo"
#define DEF_FL_BWINDOW_SYS_BTN_TEXT_CLOSE           "@-11+"

////////////////////////////////////////////////////////////////////////////////

Fl_ClientGroupTitle::Fl_ClientGroupTitle( int X, int Y, int W, int H, const char* T )
 : Fl_Box( X, Y, W, H, T ),
   bwin( NULL ),
   grabbed( false )
{
    Fl_Box::box( FL_NO_BOX );
    align( FL_ALIGN_LEFT | FL_ALIGN_CLIP | FL_ALIGN_INSIDE );
}

Fl_ClientGroupTitle::~Fl_ClientGroupTitle()
{
}

int Fl_ClientGroupTitle::handle( int e )
{
    switch ( e )
    {
        case FL_PUSH:
            if ( bwin != NULL )
            {
                int mbtn = Fl::event_button();

                if ( mbtn == FL_LEFT_MOUSE )
                {
                    if ( grabbed == false )
                    {
                        grabbed = true;

                        Fl::get_mouse( grab_x, grab_y );

                        //grab_x += bwin->x();
                        //grab_y += bwin->y();

                        grab_x -= bwin->x();
                        grab_y -= bwin->y();

                        return 1;
                    }
                }
            }
            break;

        case FL_LEAVE:
            if ( bwin != NULL )
            {
                if ( grabbed == true )
                {
                    grabbed = false;

                    return 1;
                }
            }
            break;

        case FL_RELEASE:
            if ( bwin != NULL )
            {
                int mbtn = Fl::event_button();

                if ( mbtn > 0 )
                {
                    if ( grabbed == true )
                    {
                        grabbed = false;

                        return 1;
                    }
                }
            }
            break;

        case FL_DRAG:
            if ( bwin != NULL )
            {
                if ( grabbed == true )
                {
                    int cur_x = 0;
                    int cur_y = 0;

                    Fl::get_mouse( cur_x, cur_y );

                    if ( bwin != NULL )
                    {
                        bwin->procwindowmove( cur_x - grab_x, cur_y - grab_y );

                        Fl_Widget* pparent = bwin->parent();

                        if ( pparent != NULL )
                        {
                            pparent->redraw();
                        }

                        return 1;
                    }
                }
            }
            break;
    }

    return Fl_Box::handle( e );
}

////////////////////////////////////////////////////////////////////////////////

Fl_ClientGroup::Fl_ClientGroup( int X, int Y, int W, int H, const char* T )
 : Fl_Group( X, Y, W, H ),
   boxWindowIcon( NULL ),
   boxWindowTitle( NULL ),
   grpInnerWindow( NULL ),
   sizegripped( false ),
   maximized_fs( false ),
   singlewindowtype( 0 ),
   cbOnMove( NULL ),
   defaultboxtype( FL_NO_BOX )
{
    boxWindowButtons[0] = NULL;
    boxWindowButtons[1] = NULL;
    boxWindowButtons[2] = NULL;

    Fl_Group::box( FL_THIN_UP_BOX );

    // Create inner components.
    Fl_Group::begin();

    int box_wh = DEF_Fl_ClientGroup_ICON_BOX_WH;

    Fl_Group* fbwGroupTitleDivAll = new Fl_Group( X, Y, W, box_wh );
    if ( fbwGroupTitleDivAll != NULL )
    {
        fbwGroupTitleDivAll->begin();
    }

    Fl_Group* fbwGroupTitleDiv0 = new Fl_Group( X, Y, box_wh, box_wh );
    if ( fbwGroupTitleDiv0 != NULL )
    {
        fbwGroupTitleDiv0->begin();
    }

    boxWindowIcon = new Fl_Box( X, Y, box_wh, box_wh );
    if ( boxWindowIcon != NULL )
    {
        boxWindowIcon->box( defaultboxtype );
        boxWindowIcon->labelsize( DEF_Fl_ClientGroup_LABEL_SIZE );
    }

    if ( fbwGroupTitleDiv0 != NULL )
    {
        fbwGroupTitleDiv0->end();
    }

    int box_l = X + W - ( DEF_Fl_ClientGroup_ICON_BOX_WH * 3 );
    int box_t = Y;

    Fl_Group* fbwGroupTitleDiv2 = new Fl_Group( box_l, box_t, box_wh * 3, box_wh );
    if ( fbwGroupTitleDiv2 != NULL )
    {
        fbwGroupTitleDiv2->begin();
    }

    const char* box_l_order[3] = { DEF_FL_BWINDOW_SYS_BTN_TEXT_HIDE,
                                   DEF_FL_BWINDOW_SYS_BTN_TEXT_MAXIMIZE,
                                   DEF_FL_BWINDOW_SYS_BTN_TEXT_CLOSE };
    for( int cnt=0; cnt<3; cnt++ )
    {
        boxWindowButtons[cnt] = new Fl_Button( box_l, box_t, box_wh, box_wh, box_l_order[cnt] );
        if ( boxWindowButtons[cnt] != NULL )
        {
            boxWindowButtons[cnt]->box( defaultboxtype );
            boxWindowButtons[cnt]->labelsize( DEF_Fl_ClientGroup_LABEL_SIZE );
            boxWindowButtons[cnt]->clear_visible_focus();
            boxWindowButtons[cnt]->callback( Fl_ClientGroup::Fl_ClientGroup_CB, this );
        }

        box_l += box_wh;
    }

    if ( fbwGroupTitleDiv2 != NULL )
    {
        fbwGroupTitleDiv2->end();
    }

    int title_w = W - ( DEF_Fl_ClientGroup_ICON_BOX_WH * 3 ) - box_wh;

    Fl_Group* fbwGroupTitleDiv1 = new Fl_Group( X + box_wh, box_t, title_w, box_wh );
    if ( fbwGroupTitleDiv1 != NULL )
    {
        fbwGroupTitleDiv1->begin();
    }

    boxWindowTitle = new Fl_ClientGroupTitle( X + box_wh, box_t, title_w, box_wh, T );
    if ( boxWindowTitle != NULL )
    {
        boxWindowTitle->borderlesswindow( this );
        boxWindowTitle->box( defaultboxtype );
        boxWindowTitle->labelsize( DEF_Fl_ClientGroup_LABEL_SIZE );
    }

    if ( fbwGroupTitleDiv1 != NULL )
    {
        fbwGroupTitleDiv1->end();
    }

    if ( fbwGroupTitleDivAll != NULL )
    {
        fbwGroupTitleDivAll->end();

        if ( fbwGroupTitleDiv1 != NULL )
        {
            fbwGroupTitleDivAll->resizable( fbwGroupTitleDiv1 );
        }
    }

    grpInnerWindow = new Fl_Group( X + 1, box_t + box_wh + 1, W - 2, H - box_wh - 2 );
    if ( grpInnerWindow != NULL )
    {
        this->resizable( grpInnerWindow );
    }

    Fl_Group::end();

    begin();
}

Fl_ClientGroup::~Fl_ClientGroup()
{
}

void Fl_ClientGroup::begin()
{
    if ( grpInnerWindow != NULL )
    {
        grpInnerWindow->begin();
    }
}

void Fl_ClientGroup::end()
{
    if ( grpInnerWindow != NULL )
    {
        grpInnerWindow->end();
    }
}

void reorderclient()
{
}

int Fl_ClientGroup::sizegriptest( int x, int y )
{
    int reti = 0;

    if ( ( x >= w() - 4 ) && ( x <= w() ) )
    {
        reti |= 1;
    }

    if ( ( y >= h() - 4 ) && ( y <= h() ) )
    {
        reti |= 2;
    }

    return reti;
}

int Fl_ClientGroup::handle( int e )
{
    int reti = Fl_Group::handle( e );

    switch( e )
    {
        case FL_PUSH:
            {
                int cur_x = Fl::event_x();
                int cur_y = Fl::event_y();

                unsigned szflag = sizegriptest( cur_x, cur_y );

                if ( ( sizegripped == false ) && ( szflag > 0 ) )
                {
                    sizegrip_w = w();
                    sizegrip_h = h();

                    Fl::get_mouse( sizegrip_x, sizegrip_y );

                    sizegripped = true;
                }
            }
            break;

        case FL_LEAVE:
            if ( sizegripped == true )
            {
                sizegripped = false;
            }
            break;

        case FL_MOVE:
            {
                int cur_x = Fl::event_x();
                int cur_y = Fl::event_y();

                sizegrip_flag = sizegriptest( cur_x, cur_y );

                switch( sizegrip_flag )
                {
                    case 1:
                        fl_cursor( FL_CURSOR_WE );
                        break;

                    case 2:
                        fl_cursor( FL_CURSOR_NS );
                        break;

                    case 3:
                        fl_cursor( FL_CURSOR_NWSE );
                        break;

                    default:
                        fl_cursor( FL_CURSOR_DEFAULT );
                }

                if ( cbOnMove != NULL )
                {
                    cbOnMove( this, pdOnMove );
                }
            }
            break;

        case FL_DRAG:
            if ( sizegripped == true )
            {
                int new_x;
                int new_y;

                Fl::get_mouse( new_x, new_y );

                int wsz_x = x();
                int wsz_y = y();
                int wsz_w = sizegrip_w + ( new_x - sizegrip_x );
                int wsz_h = sizegrip_h + ( new_y - sizegrip_y );

                switch( sizegrip_flag )
                {
                    case 1:
                        wsz_h = sizegrip_h;
                        break;

                    case 2:
                        wsz_w = sizegrip_w;
                        break;
                }

                resize( wsz_x, wsz_y, wsz_w, wsz_h );
            }
            break;

    }

    return reti;
}

void Fl_ClientGroup::setsinglewindow( int stype )
{
    int cmax = 2;

    if ( stype > 0  )
    {
        cmax = 3;
    }

    singlewindowtype = stype;

    for( int cnt=0; cnt<cmax; cnt++ )
    {
        if( boxWindowButtons[cnt] != NULL )
        {
            boxWindowButtons[cnt]->deactivate();
            boxWindowButtons[cnt]->hide();
        }
    }

    if ( boxWindowTitle != NULL )
    {
        int new_w = boxWindowTitle->w() + ( DEF_Fl_ClientGroup_ICON_BOX_WH * cmax );
        boxWindowTitle->resize( boxWindowTitle->x(),
                                boxWindowTitle->y(),
                                new_w,
                                boxWindowTitle->h() );
    }

}

void Fl_ClientGroup::color( Fl_Color c1, Fl_Color c2 )
{
    Fl_Group::color( c1, c2 );

    if ( boxWindowIcon != NULL )
    {
        boxWindowIcon->color( c1, c2 );
    }

    if ( boxWindowTitle != NULL )
    {
        boxWindowTitle->color( c1, c2 );
    }

    for( int cnt=0; cnt<3; cnt++ )
    {
        if( boxWindowButtons[cnt] != NULL )
        {
            boxWindowButtons[cnt]->color( c1, c2 );
        }
    }
}

void Fl_ClientGroup::color( Fl_Color bg )
{
    color( bg, bg );
}

void Fl_ClientGroup::labelcolor( Fl_Color c )
{
    Fl_Group::labelcolor( c );

    if ( boxWindowIcon != NULL )
    {
        boxWindowIcon->labelcolor( c );
    }

    if ( boxWindowTitle != NULL )
    {
        boxWindowTitle->labelcolor( c );
    }

    for( int cnt=0; cnt<3; cnt++ )
    {
        if( boxWindowButtons[cnt] != NULL )
        {
            boxWindowButtons[cnt]->labelcolor( c );
        }
    }
}

void Fl_ClientGroup::labelsize( Fl_Fontsize pix )
{
    Fl_Group::labelsize( pix );

    if ( boxWindowIcon != NULL )
    {
        boxWindowIcon->labelsize( pix );
    }

    if ( boxWindowTitle != NULL )
    {
        boxWindowTitle->labelsize( pix );
    }

    for( int cnt=0; cnt<3; cnt++ )
    {
        if( boxWindowButtons[cnt] != NULL )
        {
            boxWindowButtons[cnt]->labelsize( pix );
        }
    }
}

void Fl_ClientGroup::WCB( Fl_Widget* w )
{
    if ( w == boxWindowButtons[0] )
    {
        // Something do ...
    }

    if ( w == boxWindowButtons[1] )
    {
        if ( maximized_fs == true )
        {
            if( boxWindowButtons[0] != NULL )
            {
                boxWindowButtons[0]->show();
                boxWindowButtons[0]->activate();
            }

            if( boxWindowButtons[1] != NULL )
            {
                boxWindowButtons[1]->label( DEF_FL_BWINDOW_SYS_BTN_TEXT_MAXIMIZE );
            }

            resize( prev_fs_x, prev_fs_y, prev_fs_w, prev_fs_h );

            Fl_Widget* pParent = this->parent();

            if ( pParent != NULL )
            {
                pParent->redraw();
            }

            maximized_fs = false;
        }
        else
        {
            prev_fs_x = x();
            prev_fs_y = y();
            prev_fs_w = this->w();
            prev_fs_h = h();

            if( boxWindowButtons[0] != NULL )
            {
                boxWindowButtons[0]->deactivate();
                boxWindowButtons[0]->hide();
            }

            if( boxWindowButtons[1] != NULL )
            {
                boxWindowButtons[1]->label( DEF_FL_BWINDOW_SYS_BTN_TEXT_RETURNSIZE );
            }

            int scrn_x;
            int scrn_y;
            int scrn_w;
            int scrn_h;

            Fl_Widget* pParent = this->parent();

            if ( pParent != NULL )
            {
                resize( pParent->x(), pParent->y(), pParent->w(), pParent->h() );

                pParent->redraw();
            }

            maximized_fs = true;
        }
    }

    if ( w == boxWindowButtons[2] )
    {
        //hide();
        do_callback();
        return;
    }
}

int Fl_ClientGroup::clientarea_x()
{
    if ( grpInnerWindow != NULL )
    {
        return grpInnerWindow->x();
    }

    return 0;
}

int Fl_ClientGroup::clientarea_y()
{
    if ( grpInnerWindow != NULL )
    {
        return grpInnerWindow->y();
    }

    return 0;
}

int Fl_ClientGroup::clientarea_w()
{
    if ( grpInnerWindow != NULL )
    {
        return grpInnerWindow->w();
    }

    return w();
}

int Fl_ClientGroup::clientarea_h()
{
    if ( grpInnerWindow != NULL )
    {
        return grpInnerWindow->h();
    }

    return h();
}

Fl_Group* Fl_ClientGroup::clientarea()
{
    if ( grpInnerWindow != NULL )
    {
        return grpInnerWindow;
    }

    return this;
}

////////////////////////////////////////////////////////////////////////////////

void Fl_ClientGroup::Fl_ClientGroup_CB( Fl_Widget* w, void* p )
{
    if ( p != NULL )
    {
        Fl_ClientGroup* pw = (Fl_ClientGroup*)p;
        pw->WCB( w );
    }
}
