#include <unistd.h>

#include "coords.h"
#include "wMain.H"
#include <FL/fl_ask.H>

#include "themes.h"
#ifdef _WIN32
#include "resource.h"
#endif // _WIN32

////////////////////////////////////////////////////////////////////////////////

#define DEF_APP_NAME            "RK HDRi Studio"

////////////////////////////////////////////////////////////////////////////////

void fl_w_cb( Fl_Widget* w, void* p );
void fl_menu_cb( Fl_Widget* w, void* p );
void fl_move_cb( Fl_Widget* w, void* p );

////////////////////////////////////////////////////////////////////////////////

Fl_Menu_Item    sysMenuLists[] =
{
    {"&File", 0, 0, 0, FL_SUBMENU},
        {"&New Image",   FL_ALT + 'n',  fl_menu_cb,  (void*)10,  0 },
        {"&Open Image",  FL_ALT + 'o',  fl_menu_cb,  (void*)11,  0 },
        {"&Close Image", FL_ALT + 'c',  fl_menu_cb,  (void*)12,  FL_MENU_DIVIDER },
        {"&Quit",        0,             fl_menu_cb,  (void*)19,  0 },
        {0},
    {"&Image", 0, 0, 0, FL_SUBMENU},
        {"Apply HDR - Drago '03", 0,    fl_menu_cb,  (void*)20,  0 },
        {"Apply HDR - Reinhard '05", 0, fl_menu_cb,  (void*)21,  FL_MENU_DIVIDER },
        {"Information", 0,              fl_menu_cb,  (void*)22,  FL_MENU_INACTIVE },
        {0},
    {"&Windows", 0, 0, 0, FL_SUBMENU},
        {"Arrange windows as cards", 0, fl_menu_cb,  (void*)100, 0 },
        {"Arrange windows as tile", 0,  fl_menu_cb,  (void*)101, 0 },
        {"Fold windows all", 0,         fl_menu_cb,  (void*)102,  FL_MENU_DIVIDER },
        {0},
    {"&Help", 0, 0, 0, FL_SUBMENU},
        {"About this program", 0,       fl_menu_cb,  (void*)30, 0 },
        {"About open sources", 0,       fl_menu_cb,  (void*)31, 0 },
        {0},
    {0},
};

////////////////////////////////////////////////////////////////////////////////

wMain::wMain( int argc, char** argv )
 : _argc( argc ),
   _argv( argv ),
   mainWindow( NULL ),
   histWindow( NULL ),
   hdrWindowDrago( NULL ),
   hdrWindowReinhard( NULL )
{
    parseParams();
    createComponents();
    arrangeChildWindows();

#ifdef _WIN32
    HICON
    hIconWindowLarge = (HICON)LoadImage( fl_display,
                                         MAKEINTRESOURCE( IDC_ICON_A ),
                                         IMAGE_ICON,
                                         64,
                                         64,
                                         LR_SHARED );

    HICON
    hIconWindowSmall = (HICON)LoadImage( fl_display,
                                         MAKEINTRESOURCE( IDC_ICON_A ),
                                         IMAGE_ICON,
                                         16,
                                         16,
                                         LR_SHARED );

    SendMessage( fl_xid( mainWindow ),
                 WM_SETICON,
                 ICON_BIG,
                 (LPARAM)hIconWindowLarge );

    SendMessage( fl_xid( mainWindow ),
                 WM_SETICON,
                 ICON_SMALL,
                 (LPARAM)hIconWindowSmall );

#endif // _WIN32
}

wMain::~wMain()
{

}

int wMain::Run()
{
    return Fl::run();
}

void wMain::parseParams()
{

}

void wMain::createComponents()
{
    int ver_i[] = {APP_VERSION};
    static char wintitle[128] = {0};

    sprintf( wintitle, "%s, version %d.%d.%d.%d",
             DEF_APP_NAME,
             ver_i[0],
             ver_i[1],
             ver_i[2],
             ver_i[3] );

    mainWindow = new Fl_Double_Window( DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT, \
                                       wintitle );
    if ( mainWindow != NULL)
    {
        int cli_x = 0;
        int cli_y = 0;
        int cli_w = mainWindow->w();
        int cli_h = mainWindow->h();

        Fl_Group* grpMenu = new Fl_Group( cli_x, cli_y, cli_w, 20 );
        if ( grpMenu != NULL )
        {
            grpMenu->begin();
        }

            sysMenu = new Fl_Menu_Bar( cli_x, cli_y, cli_w, 20 );
            if ( sysMenu != NULL )
            {
                sysMenu->menu( sysMenuLists );
                sysMenu->callback( fl_w_cb, this );
            }

        if ( grpMenu != NULL )
        {
            grpMenu->end();
        }

        grpStatus = new Fl_Group( cli_x, cli_h - 20, cli_w, 20 );
        if ( grpStatus != NULL )
        {
            grpStatus->box( FL_THIN_UP_BOX );
            grpStatus->end();
        }

        sclClient = new Fl_Scroll( cli_x, cli_y + 20, cli_w, cli_h - 40 );
        if ( sclClient != NULL )
        {
            sclClient->end();
        }

        mainWindow->end();
        mainWindow->callback( fl_w_cb, this );

        // Set resize area ...
        if ( sclClient != NULL )
        {
            mainWindow->resizable( sclClient );
        }

        mainWindow->show();
    }

    /*
     * This histogram window will removed soon.
     *

    histWindow = new Fl_Double_Window( 300, 500, "Histogram" );
    if ( histWindow != NULL )
    {
        histWindow->end();
        histWindow->show();
    }
    */

    applyThemes();
}

void wMain::applyThemes()
{
    rkhdr::InitTheme();

    if ( mainWindow != NULL )
    {
        rkhdr::ApplyDWindowTheme( mainWindow );
    }

    if ( histWindow != NULL )
    {
        rkhdr::ApplyDWindowTheme( histWindow );
    }

    if ( sysMenu != NULL )
    {
        rkhdr::ApplyMenuBarTheme( sysMenu );
    }

    if ( sclClient != NULL )
    {
        rkhdr::ApplyScrollTheme( sclClient );
    }

    if ( grpStatus != NULL )
    {
        rkhdr::ApplyGroupTheme( grpStatus );
    }
}

void wMain::arrangeChildWindows()
{
    if ( histWindow != NULL )
    {
        int scrn_x = 0;
        int scrn_y = 0;
        int scrn_w = 0;
        int scrn_h = 0;

        Fl::screen_work_area( scrn_x, scrn_y, scrn_w, scrn_h );

        int new_x = mainWindow->x() + mainWindow->w();
        int new_y = mainWindow->y();

        if ( ( new_x + histWindow->w() ) > scrn_w )
        {
            new_x = scrn_w - histWindow->w();
        }

        if ( ( new_y + histWindow->h() ) > scrn_h )
        {
            new_y = scrn_h - histWindow->h();
        }

        histWindow->position( new_x, new_y );
    }
}

void wMain::new_image()
{
}

void wMain::open_image()
{
}

void wMain::close_image()
{
}

void wMain::WidgetCB( Fl_Widget* w )
{
    if ( w == mainWindow )
    {
        fl_message_title( "Program quit" );
        int retask = fl_ask( "Program may be terminated with losing current work status if you select YES, Proceed it ?" );

        if ( retask > 0 )
        {
            if ( histWindow != NULL )
            {
                histWindow->hide();
                delete histWindow;
                histWindow = NULL;
            }

            mainWindow->hide();
            delete mainWindow;
            mainWindow = NULL;
        }

        return;
    }
}

void wMain::MenuCB( Fl_Widget* w )
{
    Fl_Menu_* mw = (Fl_Menu_*)w;
    const Fl_Menu_Item* mi = mw->mvalue();
    if ( mi != NULL )
    {
        void* param = mi->user_data();
        if ( param != NULL )
        {
            if ( param == (void*)10 )
            {
                new_image();
                return;
            }

            if ( param == (void*)19 )
            {
                //mainWindow->do_callback();
                return;
            }
        }
    }
}

void wMain::MoveCB( Fl_Widget* w )
{
    if ( w == mainWindow )
    {
        arrangeChildWindows();
    }
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void fl_w_cb( Fl_Widget* w, void* p )
{
    if ( p != NULL )
    {
        wMain* wm = (wMain*)p;
        wm->WidgetCB( w );
    }
}

void fl_menu_cb( Fl_Widget* w, void* p )
{
    if ( p != NULL )
    {
        wMain* wm = (wMain*)p;
        wm->MenuCB( w );
    }
}

void fl_move_cb( Fl_Widget* w, void* p )
{
    if ( p != NULL )
    {
        wMain* wm = (wMain*)p;
        wm->MoveCB( w );
    }
}
