# RaphKay's HDR image studio #

## What is this repository for? ##

 * An Open source projet of FLTK 1.3.4 based HDR image generating studio program for multi platforms
 * Project may supports these platforms:
   1. Windows ( NT 6 or later )
   1. Linux ( Ubuntu, debian )
   1. Mac OS X

## Project built with .. ##

 * Now only supports gcc or MinGW-W64.
 * May good for IDE to build easily, Code::Blocks.
 * Mac OS X will supports other IDE.

## Using technologies ##

 * gcc/MinGW-W64 avx optimizing.
 * OpenMP, pararelle loop acceleration.

## Copyright & License ##

 * 2016, 2017, Raphael Kim
 * all referenced library depends on their license.
 * RK HDR studio following part of GPL v3.

## Imported library ##

 * FLTK-1.3.4-ts (my clone of FLTK-1.3.4)
 * libtinyxml

## Referenced library ##

 * Free Image v3.